*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot

*** Test Cases ***
Valid Register
    Open Browser To Login Page
    Input TextValue   
    Address Test
    Capture Page Screenshot
    Submit Credentials
    Capture Page Screenshot

Valid Login 
   Open Browser    ${LOGIN URL}    ${BROWSER}
   Maximize Browser Window
   Set Selenium Speed    ${DELAY}
   Login Test 


Valid Forget
    Open Browser    ${FORGET URL}      ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Valid Forget