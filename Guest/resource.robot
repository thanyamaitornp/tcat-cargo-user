*** Settings ***
Library           Selenium2Library


*** Variables ***
${SERVER}         stwww.tcatcargo.com
${BROWSER}        Chrome
${DELAY}          0.5
${REGISTER URL}      http://${SERVER}/register
${LOGIN URL}         http://${SERVER}/login
${FORGET URL}         http://${SERVER}/login


*** Keywords ***
Open Browser To Login Page
    Open Browser    ${REGISTER URL}     ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Login Page Should Be Open

Login Page Should Be Open
    Title Should Be  สมัครสมาชิก

Go To Login Page
    Go To    ${REGISTER URL} 
    Login Page Should Be Open

Input TextValue
    Input Text   id=name    นายกไก่ ทดสอบ 
    Input Text   id=email   1234@test.com
    Input Text   id=tel     0956675676
    Set Selenium Speed  0.5
    Select From List by Value      id=day      17
    Select From List by Value      id=month      6
    Select From List by Value      id=year      1995
    Input Text   id=password  123456789
    Input Text   id=password_confirmation  123456789
    Input Text   id=address  123/23 test road 

Address Test
    Set Selenium Speed  0.5
    Input Text   id=province_id   กรุงเทพมหานคร
    Press Keys   xpath=//input[@type='search']    ENTER
    Input Text   id=amphur_id   เขตจตุจักร
    Press Keys   id=amphur_id   ENTER
    Input Text   id=district_code  จอมพล
    Press Keys   id=district_code  ENTER
    click Element   id=zipcode   
    Set Selenium Speed  0
    Input Text   id=thai_shipping_method_id  EMS
    Press Keys   id=thai_shipping_method_id  ENTER
    Input Text   id=user_remark   Test

Submit Credentials
    Click Button   xpath=//button[@type='submit']
             
Login Test

    Input Text      id=email    user01@me.com
    Input Text      id=password     12345678
    Capture Page Screenshot
    Click Element    id=btn-login
    Capture Page Screenshot

Valid Forget

    Input Text      name=forgot    thanyamaitorn.p@gmail.com
    Capture Page Screenshot
    Click Button    xpath=//div[@id='__layout']/div/div/div/div/div/div/div/div[2]/div/div/form/div[2]/div/button
    Capture Page Screenshot