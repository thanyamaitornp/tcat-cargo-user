*** Settings ***
Library           Selenium2Library


*** Variables ***
${SERVER}         apwww.tcatcargo.com
${BROWSER}        Chrome
${DELAY}          0.5
${LOGIN URL}         http://${SERVER}/login
${DB URL}            http://${SERVER}/u/dashboard
${M}              2
${user_remark}          TEST_ทดสอบระบบ
${Tracking_name}        RETEST-RUNTEST2
${folderscreen}         /Users/praephat/Documents/GitHub/tcat-cargo-st/User/Screenshot/ 

*** Keywords ***
Open Browser To Login Page

    Open Browser    ${LOGIN URL}     ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Login Page Should Be Open

Login Page Should Be Open
    Title Should Be  เข้าสู่ระบบ

Login User
    Wait Until Page Contains Element  id:email  timeout=999999
    Input Text      id=email    jkrit.sr@gmail.com
    Input Text      id=password     123456
    Click Element   id=btn-login
    Capture Page Screenshot         ${folderscreen}Login-User-01.png
    Sleep   5s
    Capture Page Screenshot         ${folderscreen}Login-User-02.png

TCAT-02_Chat User
    Wait Until Page Contains Element  id:manage-link-menu-chat  timeout=999999
    Click Element     id=manage-link-menu-chat
    Wait Until Page Contains Element  id:chat_form  timeout=999999
    Choose File       id=chat_form    /Users/praephat/Documents/GitHub/tcat-cargo-st/File/6329.jpg 
    Input Text        id=message       สวัสดีค่ะ ทดสอบ 
    Click Element     id=btn-chat-send
    Capture Page Screenshot                 ${folderscreen}Chat-User-01.png
    Input Text        id=message        สอบถามรายละเอียดเพิ่มเติม
    Click Element     id=btn-chat-send
    Sleep  2s
    Capture Page Screenshot                 ${folderscreen}Chat-User-02.png

    # Press Keys    None    ARROW_DOWN    


TCAT-03-01_Tracking Search and Edit

    Wait Until Page Contains Element  id:manage-link-menu-tracking  timeout=999999  
    Click Element       id=manage-link-menu-tracking
    Set Selenium Speed      ${0.5} 
    Wait Until Page Contains Element  id:code  timeout=999999
    Input Text          id=code     TEST-00001
    Press Keys          id=no       ENTER
    Press Keys     id=status      ENTER
    # Press Keys     id=thai_in      ENTER
# #เลือกวันที่ตั้งค่ารอบที่ setting
#     Wait Until Page Contains Element  css:.btn:nth-child(2) .bi-chevron-left   timeout=999999
#     FOR    ${i}    IN RANGE    0  ${M}
#         Click Element    css=.btn:nth-child(2) .bi-chevron-left
#     END
#     # Wait Until Page Contains Element  css:.btn:nth-child(2) .bi-chevron-left   timeout=999999
#     # Click Element    css=.btn:nth-child(2) .bi-chevron-left
#     Wait Until Page Contains Element  xpath://div[3]/div[4]/span   timeout=999999
#     Click Element    xpath=//div[3]/div[4]/span
#     Sleep   2s
    Press Keys   id=calculate_type    ENTER
    Press Keys   id=shipping_method_id   ENTER
    Press Keys   id=product_type_id      ENTER
    Wait Until Page Contains Element  id:btn-trcking-search  timeout=999999
    Click Element     id=btn-trcking-search
    Capture Page Screenshot             ${folderscreen}Tracking_Search-01.png
    Sleep   2s
    Wait Until Page Contains Element   xpath://span/a  timeout=999999
    Click Element       xpath://span/a
    Capture Page Screenshot             ${folderscreen}Tracking_Search-02.png
    Wait Until Page Contains Element  id:user_remark  timeout=999999
    Input Text          id=user_remark       ${user_remark}
    Click Element       id=btn-tracking-save
    Capture Page Screenshot             ${folderscreen}Tracking_Search-03.png
    Click Element       id=link-tracking-back
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}Tracking_Search-04.png

TCAT-03-02_Tracking Add
    Wait Until Page Contains Element  id:manage-link-menu-tracking  timeout=999999  
    Click Element       id=manage-link-menu-tracking
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}Tracking_add-01.png
    Wait Until Page Contains Element  id:btn-trcking-add  timeout=999999
    Click Element       id=btn-trcking-add
#Single use data
    Input Text          id=code       ${Tracking_name}
    Wait Until Page Contains Element  id:product_type  timeout=999999
    Select From List by Value	    id=product_type    clothing
    Input Text          id=product_remark    ทดสอบโรบอท
    Input Text          id=qty               3
    Input Text          id=user_remark          ${user_remark} 
    Capture Page Screenshot             ${folderscreen}Tracking_add-02.png
    Click Element       id=btn-tracking-save
    Sleep   2s          
    Capture Page Screenshot             ${folderscreen}Tracking_add-03.png


TCAT-03-03_Tracking Add Excel
    Wait Until Page Contains Element  id:manage-link-menu-tracking  timeout=999999  
    Click Element       id=manage-link-menu-tracking
    Capture Page Screenshot                 ${folderscreen}Tracking_add-excel-01.png
    Set Selenium Speed      ${0.5}
    Wait Until Page Contains Element  id:btn-trcking-import-excel  timeout=999999
    Click Element       id=btn-trcking-import-excel   
    Wait Until Page Contains Element  id:btn-tracking-download-template  timeout=999999
    Click Element       id=btn-tracking-download-template
    Capture Page Screenshot         ${folderscreen}Tracking_add-excel-02.png
#upload file
    Sleep   2s    
    Input Text          xpath=//input[@type='file']    /Users/praephat/Documents/GitHub/tcat-cargo-st/File/tracking-user-template.xlsx
    Capture Page Screenshot         ${folderscreen}Tracking_add-excel-03.png
    Wait Until Page Contains Element  id:btn-tracking-save  timeout=999999
    Click Element       id=btn-tracking-save
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}Tracking_add-excel-04.png

TCAT-03-04_Tracking Edit
    Wait Until Page Contains Element  id:manage-link-menu-tracking  timeout=999999  
    Click Element       id=manage-link-menu-tracking
    Set Selenium Speed      0.5
    Capture Page Screenshot                 ${folderscreen}Tracking_edit-01.png
    Wait Until Page Contains Element  id:code  timeout=999999
    Input Text          id=code      ${Tracking_name}
    Wait Until Page Contains Element  id:btn-trcking-search  timeout=999999
    Click Element      id=btn-trcking-search
#     Capture Page Screenshot
#     Set Selenium Speed      0
# #เลื่อนscroll mouse
#     FOR    ${i}    IN RANGE    1   32
#         Press Keys   xpath://*[@id="__layout"]/div/div[1]/div/div/div[2]/div/div/div/div[2]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div  ARROW_RIGHT
#     END
#     Set Selenium Speed      0.5
    Capture Page Screenshot            ${folderscreen}Tracking_edit-02.png
    Wait Until Page Contains Element  xpath://span/a  timeout=999999
    Click Element   xpath://span/a
    FOR    ${i}    IN RANGE    1   3
        Press Keys   xpath://*[@id="__layout"]/div/div[1]/div/div/div[2]/div/div/div/div/form/div/div[23]  ARROW_RIGHT
    END

    Input Text          id=user_remark       ${user_remark} 
    Capture Page Screenshot         ${folderscreen}Tracking_edit-03.png
    Click Element       id=btn-tracking-save
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}Tracking_edit-04.png


TCAT-03-05_Tracking Delete

    Wait Until Page Contains Element  id:manage-link-menu-tracking  timeout=999999  
    Click Element       id=manage-link-menu-tracking
    Set Selenium Speed      ${0.5}
    Wait Until Page Contains Element  id:code  timeout=999999
#Single use data
    Input Text          id=code     UAT-DELETE
    Wait Until Page Contains Element  id:btn-trcking-search  timeout=999999
    Click Element      id=btn-trcking-search
    
    Capture Page Screenshot             ${folderscreen}Tracking_Delete-01.png
    Set Selenium Speed      ${0}
#เลื่อนscroll mouse
    FOR    ${i}    IN RANGE    1    40
        Press Keys    xpath://*[@id="__layout"]/div/div[1]/div/div/div[2]/div/div/div/div[2]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div     ARROW_RIGHT
    END
    Set Selenium Speed      ${0.5}
    Capture Page Screenshot             ${folderscreen}Tracking_Delete-02.png
    Wait Until Page Contains Element  id:btn-tracking-delete-6070092     timeout=999999
    Click Element       id=btn-tracking-delete-6070092
    Capture Page Screenshot             ${folderscreen}Tracking_Delete-03.png
    Sleep  2s
    Click Element  xpath=//button[3]


#ถ้าตกลง
#    Click Element  xpath:/html/body/div[2]/div/div[3]/button[1]    


TCAT-04-01_Bill Search
    Wait Until Page Contains Element  id:manage-link-menu-bill  timeout=999999 
    Click Element       id=manage-link-menu-bill
    Wait Until Page Contains Element  id:no  timeout=999999
    Input Text          id=no          SH-342373
    Set Selenium Speed  0.5
    Wait Until Page Contains Element  id:status   timeout=999999
    Select From List by Value   id=status     8
    Press Keys   id=thai_shipping_method_id     ENTER
    Capture Page Screenshot          ${folderscreen}Bill_Search-01.png
    Click Element   id=btn-bill-search

    Wait Until Page Contains Element  xpath://span/a  timeout=999999
    Click Element   xpath://span/a
    Capture Page Screenshot         ${folderscreen}Bill_Search-02.png




TCAT-04-02_Bill Add
#ต้องมีใบTracking status 3 อยู่แล้ว
    Wait Until Page Contains Element  id:manage-link-menu-bill  timeout=999999 
    Click Element       id=manage-link-menu-bill
    Set Selenium Speed  0.5
    Capture Page Screenshot         ${folderscreen}Bill_addd-01.png
    Wait Until Page Contains Element  id:btn-bill-add   timeout=999999
    Click Element       id=btn-bill-add
    Sleep  5s
    Wait Until Page Contains Element   xpath://tbody/tr[3]/td[1]   timeout=999999
    Click Element   xpath://tbody/tr[3]/td[1]
    Capture Page Screenshot         ${folderscreen}Bill_addd-02.png
    Select From List by Value   id=thai_shipping_id       4
    Select From List by Value   id=user_adress_id         29984
    #คูปอง
    Input Text   id=user_remark      ${user_remark}
    Capture Page Screenshot             ${folderscreen}Bill_addd-03.png
    Click Element      id=btn-bill-save
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}Bill_addd-04.png

    #ยกเลิก 
    # Capture Page Screenshot
    # Wait Until Page Contains Element   id:btn-bill-cancel   timeout=999999
    # Click Element      id=btn-bill-cancel
    # Sleep  2s
    # Capture Page Screenshot           ${folderscreen}Bill_addd-05.png
    # Click Element      xpath=//button[3]
    # Sleep  2s       

Bill_Payment
#ต้องมีใบที่อยู่ในสถานะ 2 รอชำระ

    Wait Until Page Contains Element  id:manage-link-menu-bill  timeout=999999 
    Click Element       id=manage-link-menu-bill
    Capture Page Screenshot             ${folderscreen}Bill_Payment-01.png
    Set Selenium Speed  0.5
#ค้นหาใบ
    Input Text          id=no          SH-476074
    Sleep    2s
    Click Element    id=btn-bill-search
    Capture Page Screenshot         ${folderscreen}Bill_Payment-02.png
#เลือก
    Click Element       xpath=//span/a
    Capture Page Screenshot         ${folderscreen}Bill_Payment-03.png
    Wait Until Page Contains Element  id:btn-bill-payment   timeout=999999
    Click Element       id=btn-bill-payment
    Wait Until Page Contains Element  id:checkPay   timeout=999999
    Click Element       id=checkPay
    Capture Page Screenshot
    Click Element       id=modal-btn-bill-pay
    Sleep    2s
    Capture Page Screenshot         ${folderscreen}Bill_Payment-04.png
    

TCAT-05-01_Bill Chaina Trip Tour

    Wait Until Page Contains Element  id:manage-link-menu-trip  timeout=999999 
    Click Element       id=manage-link-menu-trip
    Set Selenium Speed  0.5
    Capture Page Screenshot         ${folderscreen}Bill_Chaina-01.png
    Wait Until Page Contains Element  xpath://span/a  timeout=999999
    Click Element       xpath://span/a
    Sleep  2s
    Capture Page Screenshot         ${folderscreen}Bill_Chaina-02.png


TCAT-06-01_Topup Search
    Wait Until Page Contains Element  id:manage-link-menu-topup  timeout=999999 
    Click Element       id=manage-link-menu-topup
    Capture Page Screenshot             ${folderscreen}Topup_Search-01.png
    Sleep  2s
    Wait Until Page Contains Element  id:no  timeout=999999 
    Input Text      id=no       TU-084947
    Select From List by Value       id=status       approved
    Press Keys       id=method       ENTER
    Capture Page Screenshot             ${folderscreen}Topup_Search-02.png
    Click Element       id=btn-topup-search
    Wait Until Page Contains Element  xpath://span/a  timeout=999999 
    Click Element       xpath://span/a
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}Topup_Search-03.png
    Wait Until Page Contains Element  id:link-topup-back  timeout=999999
    Click Element       id=link-topup-back

TCAT-06-02_Topup Add
    Wait Until Page Contains Element  id:manage-link-menu-topup  timeout=999999 
    Click Element       id=manage-link-menu-topup
    Wait Until Page Contains Element  id:btn-topup-reset  timeout=999999 
    Click Element       id=btn-topup-reset
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}Topup_addd-01.png
    Wait Until Page Contains Element  id:btn-topup-add  timeout=999999    
    Click Element       id=btn-topup-add
    Wait Until Page Contains Element  id:amount  timeout=999999 
    Input Text      id=amount       10
    Select From List by Value       id=method       transfer
    Select From List by Value       id=bank         3201

    #เลือกวันที่
    Wait Until Page Contains Element  id:date__value_  timeout=999999 
    Click Element   id=date__value_
    Wait Until Page Contains Element  xpath://div[3]/span  timeout=999999 
    Click Element   xpath=//div[3]/span
    
    #เลือกเวลา
    Click Element   id=time__value_
    Click Element   xpath=//div[@id='time__dialog_']/div/footer/div/button
    Choose File     id=file    /Users/praephat/Documents/GitHub/tcat-cargo-st/File/6329.jpg 
    Input Text      id=user_remark      ${user_remark}
    Capture Page Screenshot             ${folderscreen}Topup_addd-02.png
    Click Element   id=btn-topup-create
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}Topup_addd-03.png


TCAT-07-01_Withdraw Search
    Wait Until Page Contains Element  id:manage-link-menu-withdraw  timeout=999999 
    Click Element       id=manage-link-menu-withdraw
    Capture Page Screenshot             ${folderscreen}Withdraw_Search-01.png
    Set Selenium Speed  0.5
    Wait Until Page Contains Element  id:no  timeout=999999 
    Input Text       id=no       WD-003290
    Select From List by Value       id=status       approved
    Press Keys       id=method       ENTER
    Capture Page Screenshot             ${folderscreen}Withdraw_Search-02.png
    Click Element   id=btn-withdraw-search
    Capture Page Screenshot             ${folderscreen}Withdraw_Search-03.png
    Wait Until Page Contains Element  xpath://span/a  timeout=999999 
    Click Element       xpath://span/a
    Capture Page Screenshot             ${folderscreen}Withdraw_Search-04.png
    # Wait Until Page Contains Element  id:link-withdraw-5136  timeout=999999 
    # Click Element       id=link-withdraw-back
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}Withdraw_Search-05.png

TCAT-07-02_Withdraw Add
    Wait Until Page Contains Element  id:manage-link-menu-withdraw  timeout=999999 
    Click Element       id=manage-link-menu-withdraw
    Capture Page Screenshot         ${folderscreen}Withdraw_add-01.png
    Sleep   2s
    Click Element       id=btn-withdraw-add
    Input Text          id=amount       10
    Wait Until Page Contains Element  xpath://*[@id="bank"]/option[3]    timeout=999999
    Click Element        xpath://*[@id="bank"]/option[3] 
    Input Text          id=remark        ทดสอบ 
    Capture Page Screenshot         ${folderscreen}Withdraw_add-02.png
    Click Element       id=btn-withdraw-save
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}Withdraw_add-03.png

TCAT-08-01_PointOrder Search and Edit 
    Wait Until Page Contains Element  id:manage-link-menu-point order  timeout=999999 
    Click Element       id=manage-link-menu-point order
    Capture Page Screenshot             ${folderscreen}PointOrder_Search-01.png
#Single use data กรณีตกลง        
    Input Text      id=no       PR-1085
    Press Keys       id=status       ENTER
    Wait Until Page Contains Element  id:btn-point-order-search  timeout=999999 
    Capture Page Screenshot         ${folderscreen}PointOrder_Search-02.png
    Click Element   id=btn-point-order-search
    Set Selenium Speed  1
    Wait Until Page Contains Element   xpath://span/a  timeout=999999 
    Click Element    xpath://span/a
    Wait Until Page Contains Element  id:user_remark   timeout=999999 
    Input Text      id=user_remark      ${user_remark}
    Capture Page Screenshot         ${folderscreen}PointOrder_Search-03.png
    Wait Until Page Contains Element  id:btn-point-order-update   timeout=999999 
    Click Element       id=btn-point-order-update
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}PointOrder_Search-04.png
    Set Selenium Speed  0.5

# #ยกเลิกบิล 
#     Sleep  6s
#     Capture Page Screenshot       ${folderscreen}PointOrder_Search-05.png
#     Click Element       id=btn-point-order-cancel
#     Click Element       xpath=//button[3]

# กรณีตกลง
#     Click Element       xpath=//div[3]/button
#     Sleep 2s
    
#วนลูปเลื่อนเม้าส์ขึ้นบน
    Capture Page Screenshot         ${folderscreen}PointOrder_Search-06.png
    FOR    ${i}    IN RANGE    1   5
        Press Keys  xpath://*[@id="menu-list"]/li/i    ARROW_UP
    END

TCAT-08-04_PointOrder Add

    Wait Until Page Contains Element  id:manage-link-menu-point order  timeout=999999 
    Click Element       id=manage-link-menu-point order
    Capture Page Screenshot         ${folderscreen}PointOrder_Add-01.png
    Wait Until Page Contains Element  id:btn-point-order-add  timeout=999999 
    Click Element       id=btn-point-order-add
    Capture Page Screenshot         ${folderscreen}PointOrder_Add-02.png
    Sleep   2s
    Wait Until Page Contains Element  id:btn-point-order-add-product-152  timeout=999999 
    Click Element       id=btn-point-order-add-product-152
    Wait Until Page Contains Element  id:btn-point-order-check  timeout=999999 
    Click Element       id=btn-point-order-check
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}PointOrder_Add-03.png
    Wait Until Page Contains Element  id:thai_shipping_id  timeout=999999 
    Select From List by Value       id=thai_shipping_id     2
    Wait Until Page Contains Element  id:address_id  timeout=999999 
    Select From List by Value       id=address_id       29984
    Set Selenium Speed  1
    Wait Until Page Contains Element  id:user_remark  timeout=999999 
    Input Text      id=user_remark          ${user_remark}
    Capture Page Screenshot         ${folderscreen}PointOrder_Add-04.png
    Wait Until Page Contains Element  id:btn-point-order-create  timeout=999999 
    Click Element       id=btn-point-order-create
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}PointOrder_Add-05.png



TCAT-Point Order_Payment
#ต้องมีใบที่อยู่ในสถานะ 2 
#Single use data
    Wait Until Page Contains Element  id:manage-link-menu-point order   timeout=999999 
    Click Element       id=manage-link-menu-point order
    Capture Page Screenshot        ${folderscreen}PointOrder_Payment-01.png
    Set Selenium Speed  1
    Input Text      id=no         PR-1082
    Capture Page Screenshot             ${folderscreen}PointOrder_Payment-02.png
    Wait Until Page Contains Element  id:btn-point-order-search   timeout=90
    Click Element   id=btn-point-order-search
    Click Element   xpath://span/a
    Capture Page Screenshot             ${folderscreen}PointOrder_Payment-03.png
    Wait Until Page Contains Element  id:btn-point-order-payment   timeout=90
    Click Element   id=btn-point-order-payment
    Capture Page Screenshot                ${folderscreen}PointOrder_Payment-04.png
    Click Element    id=checkPay
    Click Element    id=modal-btn-point-order-pay
    Capture Page Screenshot             ${folderscreen}PointOrder_Payment-05.png
    Set Selenium Speed  0.5


TCAT-09_Credit
    Wait Until Page Contains Element  id:manage-link-menu-credit  timeout=999999 
    Click Element       id=manage-link-menu-credit
    Capture Page Screenshot             ${folderscreen}Credit-01.png
    Set Selenium Speed  0.5
    Click Element       xpath://span/a
    Sleep  2s
    Capture Page Screenshot             ${folderscreen}Credit-02.png


TCAT-10_Nocode
    Wait Until Page Contains Element  id:manage-link-menu-nocode  timeout=999999 
    Click Element       id=manage-link-menu-nocode
    Capture Page Screenshot             ${folderscreen}Nocode-01.png
    Set Selenium Speed  0.5
    Wait Until Page Contains Element  id:nocode  timeout=999999
    Input Text          id=nocode        NC-5996764
    Press Keys     id=type       ENTER
    Capture Page Screenshot         ${folderscreen}Nocode-02.png
    Wait Until Page Contains Element  id:btn-nocode-search  timeout=999999
    Click Element       id=btn-nocode-search
    Capture Page Screenshot             ${folderscreen}Nocode-03.png


TCAT-11-03_Bank Search and Edit
    Wait Until Page Contains Element  id:manage-link-menu-bank_account  timeout=999999 
    Click Element       id=manage-link-menu-bank_account
    Capture Page Screenshot             ${folderscreen}Bank_Search-01.png
    Wait Until Page Contains Element  xpath://span/a  timeout=999999 
    Click Element       xpath://span/a
    Capture Page Screenshot             ${folderscreen}Bank_Search-02.png
    Set Selenium Speed  0.5
    Wait Until Page Contains Element  id:bank  timeout=999999 
    Select From List by Value      id=bank      KBANK
    Input Text          id=branch       ธนบุรี
    Input Text          id=title       นายก.ทดสอบ 
    Input Text          id=account      4503303945
    Select From List by Value      id=status     inactive
    Input Text          id=user_remark      ${user_remark}
    Capture Page Screenshot             ${folderscreen}Bank_Search-03.png
    Click Element       xpath=//button[@type='button']
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}Bank_Search-04.png

TCAT-11-02_Bank Add
    Wait Until Page Contains Element  id:manage-link-menu-bank_account  timeout=999999 
    Click Element       id=manage-link-menu-bank_account
    Capture Page Screenshot             ${folderscreen}Bank_add-01.png
    Wait Until Page Contains Element  id:link-bank-create  timeout=999999 
    Click Element       id=link-bank-create
    Capture Page Screenshot             ${folderscreen}Bank_add-02.png
    Set Selenium Speed  0.5
    Wait Until Page Contains Element  id:bank  timeout=999999 
    Select From List by Value      id=bank      BAY
    Input Text          id=branch       บางนา
    Input Text          id=title       นายข.ทดสอบ 
    Input Text          id=account      9203303920
    Select From List by Value      id=status     inactive
    Input Text          id=user_remark      ${user_remark}
    Capture Page Screenshot             ${folderscreen}Bank_add-03.png
    Click Element       xpath=//button[@type='button']
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}Bank_add-04.png

TCAT-12-03_Address Search and Edit
    Wait Until Page Contains Element  id:manage-link-menu-address  timeout=999999 
    Click Element       id=manage-link-menu-address
    Capture Page Screenshot             ${folderscreen}Address_Search-01.png
    Wait Until Page Contains Element  id:link-address-40223  timeout=999999
    Click Element       id=link-address-40223
    Capture Page Screenshot             ${folderscreen}Address_Search-02.png
    Set Selenium Speed  0.5
    Wait Until Page Contains Element  id:name  timeout=999999
    Input Text          id=name     ชื่อผู้รับDryrun
    Input Text          id=tel      0812131353
    Input Text          id=address     19/445 Bravo Street
    Input Text          id=province_id      ชลบุรี    
    Press Keys          id=province_id      ENTER
    Input Text          id=amphur_id    ศรีราชา  
    Press Keys          id=amphur_id      ENTER   
    Input Text          id=district_code    ทุ่งสุขลา
    Press Keys          id=district_code      ENTER  
    click Element       id=zipcode   
    Select From List by Value      id=status     inactive
    Input Text          id=user_remark      ${user_remark} 
    Capture Page Screenshot             ${folderscreen}Address_Search-03.png
    Click Element       xpath=//button[@type='submit']
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}Address_Search-04.png

TCAT-12-02_Address Add
    Wait Until Page Contains Element  id:manage-link-menu-address  timeout=999999 
    Click Element       id=manage-link-menu-address
    Capture Page Screenshot             ${folderscreen}Address_Add-01.png
    Wait Until Page Contains Element  id:link-address-create  timeout=999999
    Click Element       id=link-address-create
    Capture Page Screenshot             ${folderscreen}Address_Add-02.png
    Set Selenium Speed  0.5
    Wait Until Page Contains Element  id:name  timeout=999999
    Input Text          id=name     ชื่อของผู้รับ_ทดสอบ
    Input Text          id=tel      0678775699
    Input Text          id=Address     10/5 Bravo Street12
    Input Text          id=province_id      นนทบุรี    
    Press Keys          id=province_id      ENTER
    Input Text          id=amphur_id    บางบัวทอง  
    Press Keys          id=amphur_id      ENTER   
    Input Text          id=district_code    บางบัวทอง
    Press Keys          id=district_code      ENTER  
    click Element       id=zipcode   
    Select From List by Value      id=status     inactive
    Input Text          id=user_remark      ${user_remark}
    Capture Page Screenshot             ${folderscreen}Address_Add-03.png
    Click Element       xpath=//button[@type='submit']
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}Address_Add-04.png


TCAT-15-01_Line
# ทำเลื่อนมาที่เมนู
    FOR    ${i}    IN RANGE    1   3
        Press Keys  xpath://*[@id="__layout"]/div/div[1]/div/div/div[1]/aside/div/div    ARROW_DOWN
    END
    Wait Until Page Contains Element  id:manage-link-menu-line-notify  timeout=999999 
    Click Element       id=manage-link-menu-line-notify
    Capture Page Screenshot         ${folderscreen}Line-01.png
    Click Element       xpath=//button[@type='button']
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}Line-02.png
    Go Back


TCAT-13-01_Account Profile
    Wait Until Page Contains Element  id:manage-link-menu-profile  timeout=999999 
    Click Element       id=manage-link-menu-profile
    Capture Page Screenshot         ${folderscreen}Account_Profile-01.png
    Select From List by Value      id=day       5
    Select From List by Value      id=month     6
    Select From List by Value      id=year      1993
    Input Text      id=tel2     0972727871
    Input Text          id=thai_shipping_method_id      EMS
    Press Keys          id=thai_shipping_method_id      ENTER  
    Select From List by Value      id=sms_notification      inactive
    Input Text          id=user_remark       ${user_remark}
    Capture Page Screenshot         ${folderscreen}Account_Profile-02.png
    Click Element       id=btn-submit-password
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}Account_Profile-03.png


TCAT-13-02_Account Password
    Wait Until Page Contains Element  id:manage-link-menu-profile  timeout=999999 
    Click Element       id=manage-link-menu-profile
    Capture Page Screenshot         ${folderscreen}Account_Password-01.png
    Input Text         id=password               123456       
    Input Text          id=set_password_new          123456    
    Input Text          id=set_password_confirmation_new       123456
    Capture Page Screenshot         ${folderscreen}Account_Password-02.png
    Click Element       id=btn-submit-password
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}Account_Password-03.png

TCAT-14-01_View Point 
    Wait Until Page Contains Element  id:link-u-point  timeout=999999 
    Click Element       id=link-u-point
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}View_Point-01.png



TCAT-16-01_Coupon Bill Search
    Wait Until Page Contains Element  id:manage-link-menu-coupon_buyer    timeout=999999 
    Click Element       id=manage-link-menu-coupon_buyer
    Capture Page Screenshot             ${folderscreen}Coupon_Bill_Search-01.png
    Sleep   2s
    Press Keys      id=no          ENTER
    Wait Until Page Contains Element  id:status    timeout=999999
    Select From List by Value      id:status      cancel
    Sleep   2s
    Wait Until Page Contains Element  id:btn-coupon-buyer-search     timeout=999999 
    Click Element       id=btn-coupon-buyer-search
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_Search-02.png
    

TCAT-16-02_Coupon Bill Add

    Wait Until Page Contains Element  id:manage-link-menu-coupon_buyer    timeout=999999 
    Click Element       id=manage-link-menu-coupon_buyer
    Sleep   2s 
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_add-01.png
    Wait Until Page Contains Element  id:btn-coupon-buyer-add      timeout=999999 
    Click Element       id=btn-coupon-buyer-add
    Sleep   2s
    Wait Until Page Contains Element  id:btn-coupon-buyer-add-coupon-8     timeout=999999 
    Click Element       id=btn-coupon-buyer-add-coupon-8
    Sleep   2s
    Wait Until Page Contains Element  id:user_remark
    Input Text          id:user_remark          ${user_remark}
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_add-02.png
    Wait Until Page Contains Element  id:btn-submit     timeout=999999 
    Click Element       id=btn-submit
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_add-03.png


TCAT-16-03_Coupon Bill Edit Remark
    Wait Until Page Contains Element  id:manage-link-menu-coupon_buyer    timeout=999999 
    Click Element       id=manage-link-menu-coupon_buyer
    Sleep   2s 
    Capture Page Screenshot             ${folderscreen}Coupon_Bill_EditRemark-01.png
    Wait Until Page Contains Element   xpath=//span/a
    Click Element       xpath=//span/a
    Sleep   2s
    Wait Until Page Contains Element  id:user_remark
    Input Text          id:user_remark          ${user_remark}
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_EditRemark-02.png
    FOR    ${i}    IN RANGE    1   3
        Press Keys   xpath://*[@id="__layout"]/div/div[1]/div/div/div[2]/div/div/div/div/form/div/div[7]  ARROW_DOWN
    END
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_EditRemark-03.png
    Sleep   2s
    Wait Until Page Contains Element  id:btn-point-order-update    timeout=999999 
    Click Element       id=btn-point-order-update
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_EditRemark-04.png

TCAT-16-04_Coupon Bill payment
    Wait Until Page Contains Element  id:manage-link-menu-coupon_buyer    timeout=999999 
    Click Element       id=manage-link-menu-coupon_buyer
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_payment-01.png
    Sleep   2s 
    Input Text      id=no          CB-001046
    Wait Until Page Contains Element  id:btn-coupon-buyer-search     timeout=999999 
    Click Element       id=btn-coupon-buyer-search
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_payment-02.png
    Wait Until Page Contains Element            xpath=//span/a
    Click Element       xpath=//span/a
    Sleep   2s
    FOR    ${i}    IN RANGE    1   3
        Press Keys   xpath://*[@id="__layout"]/div/div[1]/div/div/div[2]/div/div/div/div/form/div/div[7]  ARROW_DOWN
    END
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_payment-03.png
    Wait Until Page Contains Element  id:btn-coupon-buyer-payment    timeout=999999 
    Click Element       id:btn-coupon-buyer-payment
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_payment-04.png
    Wait Until Page Contains Element  id:checkPay    timeout=999999 
    Click Element       id:checkPay
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_payment-05.png
    Wait Until Page Contains Element  id:modal-btn-coupon-buyer-pay    timeout=999999 
    Click Element       id:modal-btn-coupon-buyer-pay
    Sleep   2s
    Capture Page Screenshot         ${folderscreen}Coupon_Bill_payment-06.png


TCAT-17-01_Coupon Display
    Wait Until Page Contains Element  id:manage-link-menu-coupon     timeout=999999 
    Click Element       id=manage-link-menu-coupon
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}Coupon_Display-01.png



TCAT-18-01_Coupon Free Display
    Wait Until Page Contains Element    id:link-couponfree       timeout=999999 
    Click Element       id:link-couponfree
    Sleep   2s
    Capture Page Screenshot             ${folderscreen}Coupon_Free_Display-01.png
    Sleep   2s
    Set Selenium Speed  0
    FOR    ${i}    IN RANGE    1   8
        Press Keys   xpath://*[@id="__layout"]/div/div[1]/div  ARROW_DOWN
    END
    Set Selenium Speed  0.5
    Wait Until Page Contains Element    xpath=//div[@id='list-free-coupon-undefined-8']/div[2]/div/div[4]/div/button       timeout=999999 
    Click Element       xpath=//div[@id='list-free-coupon-undefined-8']/div[2]/div/div[4]/div/button
    Capture Page Screenshot             ${folderscreen}Coupon_Free_Display-02.png
    Sleep   2s  